CON
  WORD_MESSAGE_LENGTH = 8

PUB ReverseBits(i, num) | j, k
  j := 0
  k := 0
  repeat while (k < num)
    j :=(j << 1) | (i & 1)
    i >>= 1
    k++

  return j

PUB RtuCrc(value, crc) | nextCrc
  nextCrc := crc ^ ReverseBits(value, 8) << 8

  repeat 8
    if nextCrc & $8000
      nextCrc := nextCrc << 1 ^ $8005
    else
      nextCrc := nextCrc << 1

  return nextCrc
      
PUB RtuMessage(device, address, function, data, dataLength, message) | crc  
  byte[message++] := device  
  byte[message++] := function
  
  byte[message++] := byte[@address + 1]
  byte[message++] := byte[@address]

  crc := RtuCrc(device, $ffff)
  crc := RtuCrc(function, crc)
  crc := RtuCrc(byte[@address + 1], crc)
  crc := RtuCrc(byte[@address], crc)
                   
  repeat dataLength
    crc := RtuCrc(byte[data], crc)
    byte[message++] := byte[data++]

  crc := ReverseBits(crc, 16)
                    
  byte[message++] := byte[@crc]
  byte[message++] := byte[@crc + 1]

PUB RtuMessageWord(device, address, function, value, message) | data
  byte[@data] := byte[@value + 1]
  byte[@data + 1] := byte[@value]
  RtuMessage(device, address, function, @data, 2, message)

PUB ReadHoldingRegister(device, register, amount, message) | data
  RtuMessageWord(device, register, $03, amount, message)
  return WORD_MESSAGE_LENGTH
    
PUB WriteHoldingRegister(device, register, value, message) | data
  RtuMessageWord(device, register, $06, value, message)
  return WORD_MESSAGE_LENGTH